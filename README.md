CSV Product Import (Symfony 3)
========================

An example of importing a CSV into DB or array, using
a Symfony 3 console command and [PortPHP][1].

``` language-bash
➜  php bin/console doctrine:database:create
➜  php bin/console doctrine:schema:update --force
➜  php bin/console products:import csv /var/www/import.dev/app/Resources/stock.csv
```

Use --test option for import into array.
``` language-bash
➜  php bin/console products:import csv /var/www/import.dev/app/Resources/stock.csv --test
```

Run tests:
``` language-bash
➜  php vendor/bin/phpunit
```

[1]: https://portphp.readthedocs.io/en/latest/