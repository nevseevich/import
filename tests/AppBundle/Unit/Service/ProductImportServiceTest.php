<?php

namespace Tests\AppBundle\Unit\Service;

use AppBundle\Service\ProductImportService;
use Doctrine\ORM\EntityManager;
use Port\Reader\ArrayReader;
use Port\Writer\ArrayWriter;
use Port\Steps\StepAggregator as Workflow;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductImportServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ProductImportService
     */
    protected $productService;

    protected function setUp()
    {
        $entityManager = $this->getEntityManager();
        $validator = $this->getValidator();

        $this->productService = new ProductImportService($entityManager, $validator);
    }

    /**
     * Get Mock of Entity Manager
     */
    protected function getEntityManager()
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Get Mock of Validator
     */
    protected function getValidator()
    {
        return $this->getMockBuilder(ValidatorInterface::class)->getMock();
    }

    /**
     * Test filter step
     */
    public function testFilterStep()
    {
        $testArrayReader = new ArrayReader([
            [
                'productCode' => 'P0001',
                'stock' => '10',
                'productCost' => '300',
            ],
            [
                'productCode' => 'P0002',
                'stock' => '12',
                'productCost' => '4',
            ],
            [
                'productCode' => 'P0003',
                'stock' => '8',
                'productCost' => '3',
            ],
            [
                'productCode' => 'P0004',
                'stock' => '3',
                'productCost' => '1',
            ],
        ]);
        $arrayWriterResults = [];
        $writer = new ArrayWriter($arrayWriterResults);
        $workflow = new Workflow($testArrayReader);
        $workflow->setSkipItemOnFailure(true);

        $filterStep = self::getFilterStep($this->productService, 'getFilterStep', []);

        $workflow
            ->addStep($filterStep)
            ->addWriter($writer)
            ->process();

        $arrayExpected = [
            0 => [
                "productCode" => "P0001",
                "stock" => "10",
                "productCost" => "300",
            ],
            1 => [
                "productCode" => "P0002",
                "stock" => "12",
                "productCost" => "4",
            ]
        ];

        $this->assertEquals($arrayExpected, $arrayWriterResults);
    }

    /**
     * @param $obj
     * @param $name
     * @param array $args
     * @return mixed
     */
    public static function getFilterStep($obj, $name, array $args)
    {
        $class = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }

}