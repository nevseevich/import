<?php

namespace Tests\AppBundle\Unit\Factory;

use AppBundle\Exception\FileTypeNotFoundException;
use AppBundle\Factory\FileReaderFactory;
use AppBundle\Service\FileReader\CsvReader;
use Port\Csv\CsvReader as PortCsvReader;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class FileReaderFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test file reader with invalid file format
     */
    public function testFileReaderWithInvalidFileType()
    {
        $type = 'txt';
        $this->setExpectedException(FileTypeNotFoundException::class);
        FileReaderFactory::getFileReader($type);
    }

    /**
     * Test file reader with valid file type
     */
    public function testFileReaderWithValidType()
    {
        $type = 'csv';
        $reader = FileReaderFactory::getFileReader($type);
        $this->assertInstanceOf(CsvReader::class, $reader);
    }

    /**
     * Test csv reader with not exist file
     */
    public function testCsvReaderWithNotExistFile()
    {
        $filePath = 'file.csv';
        $this->setExpectedException(FileNotFoundException::class);
        (new CsvReader())->read($filePath);
    }

    /**
     * Test CSV reader with exist file
     */
    public function testCsvReaderWithExistFile()
    {
        $filePath = __DIR__ . '/../../Fixtures/stock.csv';
        $file = (new CsvReader())->read($filePath);

        $this->assertInstanceOf(PortCsvReader::class, $file);
    }


}