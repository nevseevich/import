<?php

namespace Tests\AppBundle\Functional;

use AppBundle\Command\ProductsImportCommand;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductImportCommandTest extends KernelTestCase
{
    /**
     * @var CommandTester
     */
    private $commandTester;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        $app = new Application($kernel);
        $app->add(new ProductsImportCommand());

        $command = $app->find('products:import');

        $this->commandTester = new CommandTester($command);
    }

    /**
     * Test command execute
     */
    public function testExecute()
    {
        $this->commandTester->execute([
            'fileType' => 'csv',
            'filePath' => __DIR__ . '/../Fixtures/test_correct.csv',
            '--test' => true
        ]);
        $this->assertEquals('Total processed: 1, Successful imported: 1, Skipped: 0' . PHP_EOL .
            'Detail about skipped products: ' . PHP_EOL,
            $this->commandTester->getDisplay());
    }


}