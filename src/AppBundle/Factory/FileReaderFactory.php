<?php

namespace AppBundle\Factory;

use AppBundle\Exception\FileTypeNotFoundException;
use AppBundle\Service\FileReader\CsvReader;

class FileReaderFactory
{
    /**
     * Create statically desired FileReader
     *
     * @param string $type
     *
     * @return CsvReader|null
     * @throws FileTypeNotFoundException
     */
    public static function getFileReader($type)
    {
        $instance = null;
        switch ($type) {
            case 'csv':
                $instance = new CsvReader();
                break;
            default:
                throw new FileTypeNotFoundException();
        }

        return $instance;
    }

}