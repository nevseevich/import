<?php

namespace AppBundle\Factory;

interface FileReaderInterface
{
    /**
     * Read file
     *
     * @param $file
     * @return mixed
     */
    public function read($file);
}