<?php

namespace AppBundle\Exception;

class FileTypeNotFoundException extends \Exception
{
}