<?php

namespace AppBundle\DTO;

class ProductImportError
{
    /**
     * @var string
     */
    private $productCode;

    /**
     * @var string
     */
    private $field;

    /***
     * @var string
     */
    private $message;

    /**
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @param string $productCode
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * ProductImportError constructor.
     *
     * @param $productCode
     * @param $field
     * @param $message
     */
    public function __construct($productCode, $field, $message)
    {
        $this->productCode = $productCode;
        $this->field = $field;
        $this->message = $message;
    }

    /**
     * Get full report message
     *
     * @return string
     */
    public function getReportMessage()
    {
        return sprintf('Product code: %s, Field: %s, Message: %s',
            $this->getProductCode(),
            $this->getField(),
            $this->getMessage()
        );
    }
}
