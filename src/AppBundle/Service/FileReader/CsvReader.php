<?php

namespace AppBundle\Service\FileReader;

use AppBundle\Factory\FileReaderInterface;
use Port\Csv\CsvReader as PortCsvReader;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class CsvReader implements FileReaderInterface
{
    /**
     * Read csv file
     *
     * @param string $file
     *
     * @return PortCsvReader
     */
    public function read($file)
    {
        try {
            $file = new \SplFileObject($file);
            $readerInstance = new PortCsvReader($file);
            return $readerInstance;
        } catch (\Exception $e) {
            throw new FileNotFoundException();
        }
    }

}