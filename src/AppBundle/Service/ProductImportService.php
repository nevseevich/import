<?php

namespace AppBundle\Service;

use AppBundle\DTO\ProductImportError;
use AppBundle\Entity\Product;
use Port\Csv\CsvReader;
use Port\Result;
use Port\Steps\Step\FilterStep;
use Port\Steps\Step\ValidatorStep;
use Port\Steps\Step\ValueConverterStep;
use Port\Writer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Port\Steps\StepAggregator as Workflow;
use Port\Steps\Step\MappingStep;
use Symfony\Component\Validator\ConstraintViolation;

class ProductImportService
{
    const HEADER_ROW_NUMBER = 0;

    const PRODUCT_MIN_COST = 5;
    const PRODUCT_MAX_COST = 1000;
    const PRODUCT_MIN_STOCK = 10;

    const MAPPING_HEADERS = [
        '[Product Code]' => '[productCode]',
        '[Product Name]' => '[productName]',
        '[Product Description]' => '[productDescription]',
        '[Stock]' => '[stock]',
        '[Cost in GBP]' => '[productCost]',
        '[Discontinued]' => '[timeDiscontinued]',
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var
     */
    protected $totalProcessedCount = 0;

    /**
     * @var
     */
    protected $successCount = 0;

    /**
     * @var
     */
    protected $errorCount = 0;

    /**
     * @var array
     */
    protected $workflowErrors = [];

    /**
     * @var array
     */
    protected $filterErrors = [];

    /**
     * ProductImportService constructor.
     *
     * @param EntityManager $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManager $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @return mixed
     */
    public function getTotalProcessedCount()
    {
        return $this->totalProcessedCount;
    }

    /**
     * @return mixed
     */
    public function getSuccessCount()
    {
        return $this->successCount;
    }

    /**
     * @return mixed
     */
    public function getErrorCount()
    {
        return $this->errorCount;
    }

    /**
     * @return mixed
     */
    public function getWorkflowErrors()
    {
        return $this->workflowErrors;
    }

    /**
     * @return mixed
     */
    public function getFilterErrors()
    {
        return $this->filterErrors;
    }

    /**
     * Get all import errors
     *
     * @return array
     */
    public function getAllImportErrors()
    {
        return array_merge($this->getWorkflowErrors(), $this->getFilterErrors());
    }

    /**
     * Run import of products
     *
     * @param CsvReader $reader
     * @param Writer $writer
     */
    public function runImport(CsvReader $reader, Writer $writer)
    {
        $reader->setHeaderRowNumber(self::HEADER_ROW_NUMBER);
        $reader->setStrict(false);
        $mappingStep = new MappingStep(self::MAPPING_HEADERS);

        $valuesConverterStep = $this->getValueConverterStep();
        $validationStep = $this->getValidatorStep();
        $filterStep = $this->getFilterStep();

        $workflow = new Workflow($reader);
        $workflow->setSkipItemOnFailure(true);

        $result = $workflow
            ->addStep($mappingStep, 4)
            ->addStep($valuesConverterStep, 3)
            ->addStep($validationStep, 2)
            ->addStep($filterStep, 1)
            ->addWriter($writer)
            ->process();

        $this->calculateErrorsCount($result);

        if ($result->hasErrors()) {
            $this->aggregateWorkflowErrors($result->getExceptions());
        }
    }

    /**
     * Get ValueConverterStep
     *
     * @return ValueConverterStep
     */
    protected function getValueConverterStep()
    {
        $valuesConverterStep = new ValueConverterStep();
        $valuesConverterStep
            ->add('[timeDiscontinued]', function ($item) {
                return $item === 'yes' ? new \DateTime() : null;
            })
            ->add('[productCost]', function ($item) {
                return trim(preg_replace('/^\D*/', '', $item));
            });

        return $valuesConverterStep;
    }

    /**
     * Get ValidatorStep
     *
     * @return ValidatorStep
     */
    protected function getValidatorStep()
    {
        $validationStep = new ValidatorStep($this->validator);
        $validationStep->throwExceptions();

        //adding Asserts rules from entity for validator step
        foreach ($this->getValidationRules() as $field => $rules) {
            foreach ($rules as $rule) {
                $validationStep->add($field, $rule);
            }
        }

        return $validationStep;
    }

    /**
     * Get FilterStep
     *
     * @return FilterStep
     */
    protected function getFilterStep()
    {
        $filterDuplicateStep = $this->getFilterDuplicateStep();
        $filterImportRuleStep = $this->getFilterImportRuleStep();
        $filterStep = new FilterStep();
        $filterStep->add($filterDuplicateStep);
        $filterStep->add($filterImportRuleStep);

        return $filterStep;
    }

    /**
     * Calculate summary import errors
     *
     * @param $result
     */
    protected function calculateErrorsCount(Result $result)
    {
        $filterErrorsCount = count($this->getFilterErrors());
        $this->totalProcessedCount = $result->getTotalProcessedCount() + $filterErrorsCount;
        $this->successCount = $result->getSuccessCount();
        $this->errorCount = $result->getErrorCount() + $filterErrorsCount;
    }

    /**
     * Filter for skipping import rules
     */
    public function getFilterImportRuleStep()
    {
        $filter = function ($item) {
            if ($item['productCost'] < self::PRODUCT_MIN_COST && $item['stock'] < self::PRODUCT_MIN_STOCK) {
                $message = 'The field productCost must be less than' . self::PRODUCT_MIN_COST . ' and field Stock must be less than ' . self::PRODUCT_MIN_STOCK;
                $this->filterErrors[] = new ProductImportError($item['productCode'], '[productCost] && [Stock]',
                    $message);
                return false;
            }

            return true;
        };

        return $filter;
    }

    /**
     * Filter for skipping duplicate items by productCode
     *
     * @return \Closure
     */
    public function getFilterDuplicateStep()
    {
        $tempItems = [];

        $filter = function ($item) use (&$tempItems) {
            if (in_array($item['productCode'], $tempItems)) {
                $message = 'Such productCode is already exist. It is duplicate';
                $this->filterErrors[] = new ProductImportError($item['productCode'], '[productCode]', $message);
                return false;
            } else {
                array_push($tempItems, $item['productCode']);

                return true;
            }
        };

        return $filter;
    }

    /**
     * Get all validations rules from entity meta
     */
    protected function getValidationRules()
    {
        $rules = [];

        /**
         * @var $metadata \Symfony\Component\Validator\Mapping\ClassMetadata
         */
        $metadata = $this->validator->getMetadataFor(new Product());

        foreach ($metadata->properties as $key => $value) {
            foreach ($value->getConstraints() as $rule) {
                $rules[$key][] = $rule;
            }
        }

        return $rules;
    }

    /**
     * Aggregate all validations errors
     *
     * @param $exceptions
     */
    protected function aggregateWorkflowErrors($exceptions)
    {
        foreach ($exceptions as $exception) {

            /**
             * @var $violation ConstraintViolation
             */
            foreach ($exception->getViolations() as $violation) {
                $this->workflowErrors[] = new ProductImportError($violation->getRoot()['productCode'],
                    $violation->getPropertyPath(), $violation->getMessage());
            }
        }
    }

}