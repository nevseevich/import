<?php

namespace AppBundle\Service;

use Symfony\Component\Console\Output\OutputInterface;

class ProductReportService
{
    /**
     * Generate report for importing
     *
     * @param OutputInterface $output
     * @param ProductImportService $productImportService
     */
    public function doReport(OutputInterface $output, ProductImportService $productImportService)
    {
        $message = sprintf('Total processed: <info>%s</info>, Successful imported: <info>%s</info>, Skipped: <info>%s</info>',
            $productImportService->getTotalProcessedCount(),
            $productImportService->getSuccessCount(),
            $productImportService->getErrorCount()
        );
        $output->writeln($message);

        $output->writeln('Detail about skipped products: ');
        foreach ($productImportService->getAllImportErrors() as $error) {
            $output->writeln('<comment>' . $error->getReportMessage() . '</comment>');
        }
    }
}