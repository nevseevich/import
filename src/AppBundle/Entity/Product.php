<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Product
 *
 * @ORM\Table(name="tblProductData")
 * @ORM\Entity
 * @UniqueEntity("productCode")
 */
class Product
{
    /**
     * Product id
     *
     * @var integer
     *
     * @ORM\Column(name="intProductDataId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Product name
     *
     * @var string
     *
     * @Assert\NotBlank(message="This value should not be blank");
     * @Assert\Type(type="string");
     *
     * @ORM\Column(name="strProductName", type="string", length=50, nullable=false)
     */
    private $productName;

    /**
     * Product description
     *
     * @var string
     *
     * @Assert\Type(type="string")
     *
     * @ORM\Column(name="strProductDesc", type="string", length=255, nullable=false)
     */
    private $productDescription;

    /**
     * Product code
     *
     * @var string
     *
     * @Assert\NotBlank(message="This value should not be blank");
     * @Assert\Type(type="string");
     *
     * @ORM\Column(name="strProductCode", type="string", length=10, nullable=false, unique=true)
     */
    private $productCode;

    /**
     * Product added DateTime
     *
     * @var \DateTime
     *
     * @ORM\Column(name="dtmAdded", type="datetime", nullable=true)
     */
    private $timeAdded;

    /**
     * The time of discontinued product
     *
     * @var \DateTime
     *
     * @Assert\DateTime(message="This value should be a DateTime")
     *
     * @ORM\Column(name="dtmDiscontinued", type="datetime", nullable=true)
     */
    private $timeDiscontinued;

    /**
     * Product updated DateTime
     *
     * @var \DateTime
     *
     * @ORM\Column(name="stmTimestamp", type="datetime", nullable=false)
     */
    private $timeUpdated;

    /**
     * Stock number
     *
     * @var integer
     *
     * @Assert\NotBlank(message="This value should not be blank")
     * @Assert\Type(type="numeric", message="This value should be of type integer")
     *
     * @ORM\Column(name="stock", type="integer",
     *     options={"unsigned"=true, "default"=0})
     */
    private $stock;

    /**
     * Product cost
     *
     * @var float
     *
     * @Assert\NotBlank(message="This value should not be blank")
     * @Assert\Type(type="numeric", message="This value should be of type integer")
     * @Assert\LessThanOrEqual(1000, message="This product cost should be less than {{ compared_value }}.")
     *
     * @ORM\Column(name="price", type="decimal", precision=8, scale=2)
     */
    private $productCost;


    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->timeAdded = new \DateTime();
        $this->timeUpdated = new \DateTime();
    }

    /**
     * Get product id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get product name
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set product name
     *
     * @param string $productName
     *
     * @return Product
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get product description
     *
     * @return string
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * Set product description
     *
     * @param string $productDescription
     *
     * @return Product
     */
    public function setProductDescription($productDescription)
    {
        $this->productDescription = $productDescription;

        return $this;
    }

    /**
     * Get product code
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set product code
     *
     * @param string $productCode
     *
     * @return Product
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get time Added
     *
     * @return \DateTime
     */
    public function getTimeAdded()
    {
        return $this->timeAdded;
    }

    /**
     * Set time Added
     *
     * @param \DateTime $timeAdded
     *
     * @return Product
     */
    public function setTimeAdded($timeAdded)
    {
        $this->timeAdded = $timeAdded;

        return $this;
    }

    /**
     * Get timeDiscontinued
     *
     * @return \DateTime
     */
    public function getTimeDiscontinued()
    {
        return $this->timeDiscontinued;
    }

    /**
     * Set timeDiscontinued
     *
     * @param \DateTime $timeDiscontinued
     *
     * @return Product
     */
    public function setTimeDiscontinued($timeDiscontinued)
    {
        $this->timeDiscontinued = $timeDiscontinued;

        return $this;
    }

    /**
     * Get timeUpdated
     *
     * @return \DateTime
     */
    public function getTimeUpdated()
    {
        return $this->timeUpdated;
    }

    /**
     * Set timeUpdated
     *
     * @param \DateTime $timeUpdated
     *
     * @return $this
     */
    public function setTimeUpdated($timeUpdated)
    {
        $this->timeUpdated = $timeUpdated;

        return $this;
    }

    /**
     * Get stock number
     *
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set stock number
     *
     * @param int $stock
     *
     * @return Product
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get product cost
     *
     * @return float
     */
    public function getProductCost()
    {
        return $this->productCost;
    }

    /**
     * Set product cost
     *
     * @param float $productCost
     *
     * @return Product
     */
    public function setProductCost($productCost)
    {
        $this->productCost = $productCost;

        return $this;
    }

}