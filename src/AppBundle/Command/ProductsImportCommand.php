<?php

namespace AppBundle\Command;

use AppBundle\Exception\FileTypeNotFoundException;
use AppBundle\Factory\FileReaderFactory;
use Port\Doctrine\DoctrineWriter;
use Port\Writer\ArrayWriter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ProductsImportCommand extends ContainerAwareCommand
{
    const ARGUMENT_FILE_TYPE = 'fileType';
    const ARGUMENT_FILE_PATH = 'filePath';
    const OPTION_TEST_MODE = 'test';

    /**
     * Configuration of script options and arguments
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('products:import')
            ->setDescription('Import products from file');

        $this
            ->addArgument(self::ARGUMENT_FILE_TYPE, InputArgument::REQUIRED, 'Type of file: ')
            ->addArgument(self::ARGUMENT_FILE_PATH, InputArgument::REQUIRED, 'File path: ')
            ->addOption(self::OPTION_TEST_MODE, 't', InputOption::VALUE_NONE,
                'Test mode: does not store products in the database');
    }

    /**
     * Execute command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return OutputInterface|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileType = $input->getArgument(self::ARGUMENT_FILE_TYPE);
        $file = $input->getArgument(self::ARGUMENT_FILE_PATH);
        $isTestMode = $input->getOption(self::OPTION_TEST_MODE);

        try {
            $reader = FileReaderFactory::getFileReader($fileType);
        } catch (FileTypeNotFoundException $e) {
            $output->writeln('<error>File format' . $fileType . ' does not support</error>');
            return;
        } catch (FileNotFoundException $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return;
        }

        $writer = null;
        $arrayWriterResults = [];
        if ($isTestMode) {
            $writer = new ArrayWriter($arrayWriterResults);

        } else {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $writer = new DoctrineWriter($em, 'AppBundle:Product', 'productCode');
            $writer->setTruncate(false);
        }

        $readFile = $reader->read($file);

        /**
         * @var \AppBundle\Service\ProductImportService $productImportService
         */
        $productImportService = $this->getContainer()->get('app.product.import');
        $reportLoggerService = $this->getContainer()->get('app.product.report');
        $productImportService->runImport($readFile, $writer);

        $reportLoggerService->doReport($output, $productImportService);
    }

}
